# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from QQMusicSpider.items import MusicItem
import json
from scrapy.exceptions import DropItem
import MySQLdb as mysqldb

class DuplicatesPipeline(object):
    """
    根据音乐的song_id，对爬取过的音乐进行去重
    """

    def __init__(self):
        self.song_ids = set()

    def process_item(self, item, spider):
        if isinstance(item, MusicItem):
            if item['song_id'] in self.song_ids:
                raise DropItem("Duplicate item found: %s" % item)
            else:
                self.song_ids.add(item['song_id'])
                return item


class QqmusicspiderPipeline(object):
    def __init__(self):
        # music_path = "music"
        DBhostname = 'localhost'
        DBuser     = 'tjmusic'
        DBpassword = 'tjmusic'
        DBdatabase = 'tjmusic'


        self.db = mysqldb.connect(DBhostname, DBuser, DBpassword, DBdatabase)
        self.cursor = self.db.cursor()
        self.cursor.execute('SET NAMES utf8;')
        self.cursor.execute('SET CHARACTER SET utf8;')
        self.cursor.execute('SET character_set_connection=utf8;')
        # self.file = open(music_path, "w", encoding="utf8")

    def process_item(self, item, spider):
        if isinstance(item, MusicItem):
            dict_item = dict(item)
            # print(dict_item)
            # line = json.dumps(dict(item), ensure_ascii=False) + "\n"
            query = ('INSERT INTO musics (music_name,music_subtitle,singers,album_name,time_public,music_type,music_language,song_source,song_id,song_mid, music_link) SELECT ' +
                '\"' + dict_item['song_name']           + '\",' +
                '\"' + dict_item['subtitle']            + '\",' +
                '\"' + dict_item['singer_name'][0]      + '\",' +
                '\"' + dict_item['album_name']          + '\",' +
                '\"' + dict_item['song_time_public']    + '\",' +
                str( dict_item['song_type'] )           + ','   +
                str( dict_item['language'] )            + ','   +
                '\"' + 'qqmusic'                        + '\",'  +
                str( dict_item['song_id'] )             + ','   +
                '\"' + dict_item['song_mid']            + '\",' +
                '\"' + dict_item['song_url']            + '\" ' 
                # + 'FROM musics WHERE NOT EXISTS(SELECT song_id, song_mid FROM musics WHERE song_id = ' + str(dict_item['song_id']) + ' AND song_mid = \"' + dict_item['song_mid'] + '\") LIMIT 1;'
            )
            # print(query)
            # self.file.write(query)
            self.cursor.execute(query.encode("utf-8"))
            self.db.commit()
        return item

    def close_spider(self, spider):
        self.file.close()
