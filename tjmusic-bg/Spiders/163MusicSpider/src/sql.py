"""
一般 Python 用于连接 MySQL 的工具：pymysql
"""

drop_sql = '''
DROP TABLE IF EXISTS t_artists;
DROP TABLE IF EXISTS t_albums;
DROP TABLE IF EXISTS t_musics;
'''

create_sql = '''
CREATE TABLE `t_artists` (
  `artist_id` bigint(20) NOT NULL,
  `artist_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`artist_id`),
  KEY `unique_id` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_albums` (
  `album_id` bigint(20) NOT NULL,
  `artist_id` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_musics` (
  `music_id` int(20) NOT NULL,
  `music_name` varchar(255) DEFAULT NULL,
  `album_id` int(20) NOT NULL,
  PRIMARY KEY (`music_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'''

insert_sql = '''
INSERT INTO musics (music_name,  music_subtitle, singers,     album_name, time_public,  music_type, music_language,  song_source, song_id , song_mid, music_link)
SELECT              music_name, "none",          artist_name, title,      "1970-1-1", 0,          0,                "163music", music_id, "none",   "none"
FROM t_musics INNER JOIN t_albums ON t_musics.album_id = t_albums.album_id INNER JOIN t_artists ON t_albums.artist_id = t_artists.artist_id
'''

import MySQLdb as mysqldb
DBhostname = 'localhost'
DBuser     = 'tjmusic'
DBpassword = 'tjmusic'
DBdatabase = 'tjmusic'

connection = mysqldb.connect(DBhostname, DBuser, DBpassword, DBdatabase)
with connection.cursor() as cursor:
    cursor.execute('SET NAMES utf8;')
    cursor.execute('SET CHARACTER SET utf8;')
    cursor.execute('SET character_set_connection=utf8;')
connection.commit()

# 保存音乐
def insert_music(music_id, music_name, album_id):
    with connection.cursor() as cursor:
        sql = "INSERT INTO `t_musics` (`music_id`, `music_name`, `album_id`) VALUES (" + str(music_id) + ", \"" + music_name + "\", " + str(album_id) + ")"
        cursor.execute(sql.encode("utf-8"))
    connection.commit()


# 保存专辑
def insert_album(album_id, artist_id, title, img):
    with connection.cursor() as cursor:
        sql = "INSERT INTO `t_albums` (album_id, artist_id, title, img ) VALUES (" + str(album_id) + ", " + str(artist_id) + ", \"" + title + "\", \"" + img + "\")"
        cursor.execute(sql.encode("utf-8"))
    connection.commit()


# 保存歌手
def insert_artist(artist_id, artist_name):
    with connection.cursor() as cursor:
        sql = "INSERT INTO `t_artists` (`artist_id`, `artist_name`) VALUES ( " + str(artist_id) + ", \"" + artist_name + "\")"
        cursor.execute(sql.encode("utf-8"))
    connection.commit()


# 获取所有歌手的 数量
def get_all_artist_num():
    with connection.cursor() as cursor:
        sql = "SELECT count(*) as num FROM `t_artists` "
        cursor.execute(sql, ())
        return cursor.fetchone()


# 分页获取歌手的 ID
def get_artist_page(offset, size):
    with connection.cursor() as cursor:
        sql = "SELECT `artist_id` FROM `t_artists` limit %s ,%s"
        cursor.execute(sql, (offset, size))
        return cursor.fetchall()


# 获取所有专辑的 数量 歌曲到 36504028
def get_all_album_num():
    with connection.cursor() as cursor:
        sql = "SELECT count(*) as num FROM `t_albums` where album_id > 36503960 "
        cursor.execute(sql, ())
        return cursor.fetchone()


# 分页获取专辑的 ID
def get_album_page(offset, size):
    with connection.cursor() as cursor:
        sql = "SELECT `album_id` FROM `t_albums` where album_id > 36503960 limit %s ,%s"
        cursor.execute(sql, (offset, size))
        return cursor.fetchall()


# 获取所有音乐的 ID
def get_all_music():
    with connection.cursor() as cursor:
        sql = "SELECT `music_id` FROM `t_musics` ORDER BY music_id"
        cursor.execute(sql, ())
        return cursor.fetchall()


def dis_connect():
    connection.close()


# 清库
def truncate_all():
    with connection.cursor() as cursor:
        cursor.execute(drop_sql)
        cursor.execute(create_sql)
    connection.commit()

def drop_tmp_table():
    with connection.cursor() as cursor:
        cursor.execute(insert_sql)
        cursor.execute(drop_sql)
    connection.commit()

def save_all_musics():
    pass
