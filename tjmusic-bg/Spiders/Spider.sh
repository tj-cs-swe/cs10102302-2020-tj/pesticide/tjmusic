#!/bin/bash
Cur_Dir=$(pwd)
cd $Cur_Dir/QQMusicSpider && (nohup scrapy crawl qqmusic 1>/dev/null 2>&1 &);
cd $Cur_Dir/163MusicSpider && (nohup python3 main.py 1>/dev/null 2>&1 &);
