/*
 * 文件名:  Test.cpp
 * 作者:    1751400-张校
 * 概述:    可单独测试LogWriter
 */


#include "LogWriter.h"
#include <iostream>
using namespace std;

int main()
{
    LOGWRITER logwriter("1.log", LOGWRITER::LOGWRITER_APPEND);
    logwriter.Write("helloworld ");
    logwriter.Write_Line("hi");
    return 0;
}