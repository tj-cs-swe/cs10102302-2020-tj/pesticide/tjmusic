/*
 * 文件名:  LogWriter.h
 * 作者:    1751400-张校
 * 概述:    包含日志文件类 LOGWRITER 定义的头文件
 */

#include <fstream>
#include <string>

class LOGWRITER
{
private:
    std::string filename;
public:
    enum LOGWRITER_MODE {
        LOGWRITER_TRUNC = 0,    // 截断
        LOGWRITER_APPEND = 1,   // 追加
    };
    LOGWRITER(std::string, LOGWRITER_MODE);
    ~LOGWRITER();
    
    std::fstream fout;
    // 写日志
    bool Write(std::string);
    // 写一行日志
    bool Write_Line(std::string);
};