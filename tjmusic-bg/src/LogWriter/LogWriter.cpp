/*
 * 文件名:  LogWriter.h
 * 作者:    1751400-张校
 * 概述:    包含日志文件类 LOGWRITER 函数实现
 */

#include "LogWriter.h"

using namespace std;

LOGWRITER::LOGWRITER(string _filename, LOGWRITER_MODE _mode)
    : filename(_filename)
{
    if( _mode == LOGWRITER_TRUNC )
    {
        fout.open(filename.c_str(), ios::out);
        // fout.close();
    }
    else
    {
        fout.open(filename.c_str(), ios::out | ios::app);
    }
}

LOGWRITER::~LOGWRITER()
{
    fout.close();
}

bool LOGWRITER::Write(string buf)
{
    // fstream fout(filename.c_str(), ios::out | ios::app);
     if( ! fout )
        return false;
    
    fout << buf;
    // fout.close();
    return true;
}

bool LOGWRITER::Write_Line(string buf)
{
    // fstream fout(filename.c_str(), ios::out | ios::app);
    if( ! fout )
        return false;
    
    fout << buf << endl;
    // fout.close();
    return true;
}