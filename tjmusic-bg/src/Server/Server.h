/*
 * 文件�??:  Server.h
 * 作�?:    1751400-张校
 * 概述:    包含Server.cpp需要的头文件和部分数据、宏定义、事务处理函数等
 */

#ifndef _SERVER_H
#define _SERVER_H

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <map>
#include <set>
#include <array>

#include "Server_Interact.h"
#include "../Config/Config.h"
#include "../Database/Database.h"
#include "../LogWriter/LogWriter.h"
#include "../Md5/Md5.h"
#include "../Tools/Tools.h"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/epoll.h>
#include <limits.h>
#include <sys/file.h>
#include <sys/prctl.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/un.h>
#include <sys/wait.h>

/* 在这里定义宏变量和宏函数 */

// 配置文件路径
#define CONFIG_FILE_NAME "../config/server.cfg"


/* 在这里定义Server工具函数 */

typedef bool (*SERVER_FUNCTION_FP)(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
// 配置信息，定义于 Server.cpp
extern CONFIG config;
// 事务处理函数列表，定义于 Server_Functions.cpp
extern std::map<std::string, SERVER_FUNCTION_FP> server_functions;

/* 在这里定义事务处理函数和需要的数据类型、类�?? */
/* 事务处理函数的返回类型和参数类型不能改变！！�?? */

bool ServFunc_Example(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_CancelAccount(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_SignOut(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter);
bool ServFunc_SignIn(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_Register(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_SearchMusic(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_SearchCollection(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_FetchUserCollection(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_FetchCollectionDetail(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_add_user_collections(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_del_user_collections(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);
bool ServFunc_add_music_to_collection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter);
bool ServFunc_remove_music_from_collection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter);
bool ServFunc_CreateCollection(MESSAGE &, MESSAGE &, MESSAGE &, DB &, LOGWRITER &);

#endif
