/*
 * 文件名:  Server.cpp
 * 作者:    1751400-张校
 * 概述:    客户端常规交互进程
 */

#include "Server.h"

using namespace std;

CONFIG config;

int main(int argc, char **argv)
{
    // 默认配置信息
    /*
     * 在命令行中使用如 --deamon enable 可以设置运行时配置，优先级高于配置文件
     * 如果数据为 enable 则可以省略
     */
    config.Add_Config("daemon"                  , "disable"         ); // 守护进程
    config.Add_Config("ipaddr"                  , "0.0.0.0"         ); // 监听地址
    config.Add_Config("port"                    , "21345"           ); // 监听端口
    config.Add_Config("max_connected_clients"   , "1000"            ); // 最大同时连接客户端数
    config.Add_Config("timeout"                 , "10"              ); // 客户端超时时间（秒）
    config.Add_Config("server_max_buf_size"     , "8096"            ); // 服务端给每一个客户端最大缓冲区大小
    config.Add_Config("db_host"                 , "127.0.0.1"       ); // 数据库主机地址
    config.Add_Config("db_username"             , "tjmusic"         ); // 数据库用户名
    config.Add_Config("db_passwd"               , "tjmusic"         ); // 数据库密码
    config.Add_Config("db_dbname"               , "tjmusic"         ); // 数据库库名
    config.Add_Config("db_charset"              , "utf8"            ); // 数据库字符编码
    config.Add_Config("db_build"                , "disable"         ); // 运行时创建数据库
    config.Add_Config("db_build_file"           , "../db_init.sql"  ); // 数据库初始化文件
    config.Add_Config("log_file_name"           , "../log/server.log"); // 日志文件名
    config.Add_Config("log_file_append"         , "enable"          ); // 日志文件追加打开
    // 读取配置文件配置
    config.Add_Config_By_File(CONFIG_FILE_NAME);
    // 读取命令行配置
    config.Add_Config_By_Cmd_Argument(argc, argv);

    if( config["help"] == "enable" )
    {
        // cout << "Usage " << argv[0] << usage_info << endl;
        return 0;
    }

    if( config["deamon"] == "enable" )
    {
        Daemon();
        close(0);
        close(1);
        close(2);
    }

    signal(SIGCHLD, sig_chld_handle);
    // prctl(PR_SET_PDEATHSIG, SIGKILL);

    // 获取配置信息
    int max_connected_clients = atoi(config["max_connected_clients"].c_str());
    if( max_connected_clients < 1 || max_connected_clients > 10000 )
        max_connected_clients = 1000;
    int timeout = atoi(config["timeout"].c_str());
    if( timeout < 5 || timeout > 300 )
        timeout = 10;
    int server_max_buf_size = atoi(config["server_max_buf_size"].c_str());
    if( server_max_buf_size < 1024 )
        server_max_buf_size = 8096;

    // 创建日志文件对象
    LOGWRITER logwriter(config["log_file_name"], config["log_file_append"] == "enable" ? LOGWRITER::LOGWRITER_APPEND : LOGWRITER::LOGWRITER_TRUNC);

    if( config["db_build"] == "enable" )
    {
        string cmd;
        cmd.append("mysql -u").append(config["db_username"]).append(" -p").append(config["db_passwd"]).append(" < ").append(config["db_build_file"]).append("\n");
        system(cmd.c_str());
        
        logwriter.fout << Get_Time() << " [Server]: 重建数据库完成" << endl;
    }

    // 启动Server监听套接字
    int serv_sock;
    struct sockaddr_in serv_addr;

    serv_sock = socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(config["ipaddr"].c_str());
    serv_addr.sin_port = htons(atoi(config["port"].c_str()));

    int opt = 1;
    setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, (const void *)&opt, sizeof(opt));

    if( bind(serv_sock, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) == -1 )
    {
        logwriter.fout << Get_Time() << " [Server]: 套接字绑定失败: " << strerror(errno) << " ，服务器退出" << endl;
        return 0;
    }

    if( listen(serv_sock, 1000) == -1 )
    {
        logwriter.fout << Get_Time() << " [Server]: 监听启动失败: " << strerror(errno) << " ，服务器退出" << endl;
        return 0;
    }

    // 使用epoll监听
    struct epoll_event *ep_events;
    struct epoll_event event;
    int epfd, event_cnt;

    epfd = epoll_create(max_connected_clients);
    ep_events = (struct epoll_event*)malloc(sizeof(struct epoll_event) * max_connected_clients);

    event.events = EPOLLIN;
    event.data.fd = serv_sock;
    epoll_ctl(epfd, EPOLL_CTL_ADD, serv_sock, &event);

    // 创建SERVER_INTERACT对象指针map
    map<int, SERVER_INTERACT *> sock_list;
    // 创建数据库对象
    DB db(config["db_host"], config["db_username"], config["db_passwd"], config["db_dbname"], config["db_charset"]);

    size_t last_check_time = time(NULL);

    logwriter.fout << Get_Time() << " [Server]: 服务器初始化完成，开始监听" << endl;
    // 开始运行
    while( true )
    {
        size_t now_time = time(NULL); // 当前时间
        event_cnt = epoll_wait(epfd, ep_events, max_connected_clients, timeout * 1000);
        if( event_cnt == 0 )
            db.Disconnect();
        
        for( int event_i = 0; event_i < event_cnt; event_i ++ )
        {
            int sock = ep_events[event_i].data.fd;
            if( sock == serv_sock ) // 接收新连接
            {
                int clnt_sock;
                struct sockaddr_in clnt_addr;
                socklen_t clnt_addr_size = sizeof(clnt_addr);
                clnt_sock = accept(serv_sock, (struct sockaddr*) &clnt_addr, &clnt_addr_size);

                if(clnt_sock == -1)
                {
                    continue;
                }
                fcntl(clnt_sock, F_SETFL, fcntl(clnt_sock, F_GETFL) | O_NONBLOCK);

                event.events = EPOLLIN;
                event.data.fd = clnt_sock;
                epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sock, &event);
                
                sock_list[clnt_sock] = new SERVER_INTERACT(clnt_sock, inet_ntoa(clnt_addr.sin_addr), ntohs(clnt_addr.sin_port), server_max_buf_size);
                
                logwriter.fout << Get_Time() << " [Server]: 接收来自 " << sock_list[clnt_sock]->Ip_Addr() << ':' << sock_list[clnt_sock]->Port() << " 的连接，该连接被绑定至 " << clnt_sock << endl;
            }
            else // 处理连接
            {
                SERVER_INTERACT *st = sock_list[sock];
                MESSAGE read_msg, write_msg;
                bool disconnect = false;
                int msg_len = st->Read_Message(read_msg);
                if( msg_len == 0 ) // 客户端断开连接
                {
                    logwriter.fout << Get_Time() << " [" << st->Ip_Addr() << ':' << st->Port() << "](" << sock << "): 发送EOF" << endl;
                    disconnect = true;
                }
                else if( msg_len == -1 ) // 未接收完整数据
                {
                    continue;
                }
                else
                {
                    // 调用对应事务处理函数
                    
                    if( server_functions.find(read_msg["type"]) != server_functions.end() )
                    {
                        logwriter.fout << Get_Time() << " [" << st->Ip_Addr() << ':' << st->Port() << "](" << sock << "): 开始处理事务，type: " << read_msg["type"] << endl;
                        disconnect = ! (*server_functions[read_msg["type"]])(read_msg, st->data, write_msg, db, logwriter) || read_msg["connect"] != "keep alive";
                        st->Write_Message(write_msg);
                        logwriter.fout << Get_Time() << " [" << st->Ip_Addr() << ':' << st->Port() << "](" << sock << "): 处理事务结束。连接将被" << (disconnect ? "断开" : "保留") << endl;
                    }
                    else
                    {
                        logwriter.fout << Get_Time() << " [" << st->Ip_Addr() << ':' << st->Port() << "](" << sock << "): 发送非法事务，准备断开连接" << endl;
                        disconnect = true;
                    }
                }

                if( disconnect == true ) // 事务结束后是否断开连接
                {
                    logwriter.fout << Get_Time() << " [Server]: 与 " << st->Ip_Addr() << ':' << st->Port() << " (" << sock << "): 断开连接" << endl;
                    delete sock_list[sock];
                    sock_list.erase(sock);
                    epoll_ctl(epfd, EPOLL_CTL_DEL, sock, NULL);
                }
            }
        }

        // 超时检查，不需要修改
        if( now_time - last_check_time >= timeout )
        {
            last_check_time = now_time;
            map<int, SERVER_INTERACT *>::iterator iter = sock_list.begin();
            while( iter != sock_list.end() )
            {
                SERVER_INTERACT *st = iter->second;
                int sock = st->Sock();

                if( now_time > st->Last_Active_Time() + timeout ) // 超时断开连接
                {
                    logwriter.fout << Get_Time() << " [Server]: " << st->Ip_Addr() << ':' << st->Port() << " (" << st->Sock() << ") 长时间未相应，断开连接" << endl;
                    delete sock_list[sock];
                    sock_list.erase(sock);
                    epoll_ctl(epfd, EPOLL_CTL_DEL, sock, NULL);
                }
                iter ++;
            }
        }
    }
}
