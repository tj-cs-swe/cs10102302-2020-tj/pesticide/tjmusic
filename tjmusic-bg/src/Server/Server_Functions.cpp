/*
 * 文件名:  Server_Functions.h
 * 作者:    1751400-张校
 * 概述:    存放Server工具函数、常量定义和事务处理函数实现
 */

#include "Server.h"

using namespace std;

/* 在这里定义常量或static函数 */
/*
 * server_functions函数指针数组
 * 作者:    1751400-张校
 * 概述:    5个参数分别为 套接字读到的数据转化的MESSAGE、保留的MESSAGE、将写入套接字的MESSAGE、 数据库对象引用、日志文件对象引用
 */
map<string, SERVER_FUNCTION_FP> server_functions = {
    {"cancel account"   , ServFunc_CancelAccount    },
    {"sign out"         , ServFunc_SignOut          },
    {"sign in"          , ServFunc_SignIn           },
    {"register"         , ServFunc_Register         },
	{"search music"		, ServFunc_SearchMusic		},
	{"search collection", ServFunc_SearchCollection	},
	{"user collection"	, ServFunc_FetchUserCollection		},
	{"collection detail", ServFunc_FetchCollectionDetail	},
    {"add music to collection",ServFunc_add_music_to_collection},
    {"remove music from collection",ServFunc_remove_music_from_collection},
    {"add collection",ServFunc_add_user_collections},
    {"delete collection", ServFunc_del_user_collections},
    {"create collection", ServFunc_CreateCollection}
};

/* 在这里实现Server工具函数 */



/* 在这里实现事务处理函数 */

/*
 * 函数:    bool ServFunc_CancelAccount(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
 * 作者:    1751763-陈威
 * 功能:    注销用户的事务函数,注销之后会自动删除该用户的所有回话(调用SignOut).
 * 参数:
    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
 * 返回值:  是否成功注销用户(true:成功;false:出错)
 * 备注:    
 */

bool ServFunc_CancelAccount(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "cancel account";
    write_message["time"]=Get_Time();
    if( read_message["username"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 未找到必要字段\"username\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    if( read_message["session"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 未找到必要字段\"session\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    int user_id=db.Session(read_message["session"]);
    switch(user_id)
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
            break;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 认证失败 " << read_message["session"] << endl;
            write_message["type"] = "err";
            return false;
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 认证成功 " << read_message["username"] << " ，用户id为" << user_id << endl;
    }
    int cancel_ret=db.CancelAccount(read_message["username"],user_id);
    switch( cancel_ret )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
            break;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 注销成功" << read_message["username"] << " ，用户id为" << user_id << endl;
            write_message["type"] = "success";
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_CancelAccount]: 注销失败 " << read_message["username"] << " ，用户id为" << user_id << endl;
            write_message["type"] = "err";
            return false;
    }
    db.SignOut(to_string(user_id));
    return true;
}


/*
* 函数:    bool ServFunc_SignOut(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1753935-夏子寒
* 功能:    用户登出事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否登出成功(true：成功、无此会话；false：失败)
* 备注:
*/
bool ServFunc_SignOut(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "sign out";
    write_message["time"]=Get_Time();
    if( read_message["username"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 未找到必要字段\"username\"" << endl;
        write_message["type"] = "err";
        return true;
    }
    if( read_message["session"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 未找到必要字段\"session\"" << endl;
        write_message["type"] = "err";
        return true;
    }
    int user_id=db.Session(read_message["session"]);
    switch(user_id)
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 认证失败 " << read_message["session"] << endl;
            write_message["type"] = "err";
            return false;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 认证成功 " << read_message["session"] << " ，用户id:" << user_id << endl;
    }
    int signOut_ret=db.SignOut(to_string(user_id));
    switch( signOut_ret )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 登出成功" << read_message["session"] << " ，用户id为" << user_id << endl;
            write_message["type"] = "success";
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_SignOut]: 登出失败 " << read_message["session"] << " ，用户id为" << user_id << endl;
            write_message["type"] = "err";
            break;
    }
    return true;
}

/*
* 函数:    bool ServFunc_SignIn(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1751739-杨君临
* 功能:    用户登录事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否登录成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_SignIn(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "sign in";
    write_message["time"] = Get_Time();
    
    if( read_message["username"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_Example]: 未找到必要字段\"username\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    
    if( read_message["password"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_Example]: 未找到必要字段\"password\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    
    string session_code;
    int user_id = db.Login(read_message["username"], read_message["password"], session_code);
    write_message["session"] = session_code;
    switch( user_id )
    {
    case -1:
        logwriter.fout << Get_Time() << " [ServFunc_SignIn]: 数据库连接失败" << endl;
        write_message["type"] = "err";
        return false;
        break;

    case 0:
        logwriter.fout << Get_Time() << " [ServFunc_SignIn]: 登录失败 " << read_message["username"] << endl;
        write_message["type"] = "err";
        return false;
        break;

    default:
        logwriter.fout << Get_Time() << " [ServFunc_SignIn]: 找到用户 " << read_message["username"] << " ，用户id为" << user_id << endl;
        write_message["type"] = "success";
        break;
    }
    
    return true;
}

/*
* 函数:    bool ServFunc_Register(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1753917-谷贤明
* 功能:    用户注册事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否注册成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_Register(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
	write_message["origin"] = "register";
	write_message["time"] = Get_Time();
	bool ret;
	if (read_message["username"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_Register]: 未找到必要字段\"username\"" << endl;
		write_message["Error"] = "err username";
		return false;
	}

	if (read_message["password"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_Register]: 未找到必要字段\"password\"" << endl;
		write_message["type"] = "err password";
		return false;
	}

	int register_ret = db.Register(read_message["username"], read_message["password"]);
	switch (register_ret)
	{
	case -1:
		logwriter.fout << Get_Time() << " [ServFunc_Register]: 数据库连接失败" << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	case 1:
		logwriter.fout << Get_Time() << " [ServFunc_Register]: 注册失败 " << read_message["username"] << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	default://0
		logwriter.fout << Get_Time() << " [ServFunc_Register]: 注册成功 " << read_message["username"] << ","<<read_message["password"]  << endl;
		write_message["type"] = "register success";
		ret = true;
	}
	return ret;
}

/*
* 函数:    bool ServFunc_add_user_collections(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1753917-谷贤明
* 功能:    用户收藏歌单事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否收藏歌单成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_add_user_collections(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
	write_message["origin"] = "add collection";
	write_message["time"] = Get_Time();
	bool ret;
	if (read_message["collection id"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_add_user_collections]: 未找到必要字段\"collection_id\"" << endl;
		write_message["type"] = "err";
		return false;
	}
	if (read_message["username"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_add_user_collections]: 未找到必要字段\"username\"" << endl;
		write_message["type"] = "err";
		return false;
	}
	if (read_message["session"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_add_user_collections]: 未找到必要字段\"session\"" << endl;
		write_message["type"] = "err";
		return false;
	}
	int add_ret = db.add_user_collections(read_message["collection id"], read_message["username"], read_message["session"]);
	switch (add_ret)
	{
	case -1:
		logwriter.fout << Get_Time() << " [ServFunc_add_user_collections]: 数据库连接失败" << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	case 1:
		logwriter.fout << Get_Time() << " [ServFunc_add_user_collections]: 收藏歌单失败 " << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	default://0
		logwriter.fout << Get_Time() << " [ServFunc_add_user_collections]: 收藏歌单成功 " << read_message["username"] << "," << read_message["collection id"] << endl;
		write_message["type"] = "success";
		ret = true;
	}
	return ret;
}

/*
* 函数:    bool ServFunc_del_user_collections(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1753917-谷贤明
* 功能:    用户删除歌单事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否删除歌单成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_del_user_collections(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
	write_message["origin"] = "delete collection";
	write_message["time"] = Get_Time();
	bool ret;
	if (read_message["collection id"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_del_user_collections]: 未找到必要字段\"collection_id\"" << endl;
		write_message["type"] = "err";
		return false;
	}
	if (read_message["username"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_del_user_collections]: 未找到必要字段\"username\"" << endl;
		write_message["type"] = "err";
		return false;
	}
	if (read_message["session"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_del_user_collections]: 未找到必要字段\"session\"" << endl;
		write_message["type"] = "err";
		return false;
	}
	int del_ret = db.del_user_collections(read_message["collection id"], read_message["username"], read_message["session"]);
	switch (del_ret)
	{
	case -1:
		logwriter.fout << Get_Time() << " [ServFunc_del_user_collections]: 数据库连接失败" << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	case 1:
		logwriter.fout << Get_Time() << " [ServFunc_del_user_collections]: 删除歌单失败 " << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	default://0
		logwriter.fout << Get_Time() << " [ServFunc_del_user_collections]: 删除歌单成功 " << read_message["username"] << "," << read_message["collection id"] << endl;
		write_message["type"] = "success";
		ret = true;
	}
	return ret;
}


/*
* 函数:    bool ServFunc_add_music_to_collection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1753935-夏子寒
* 功能:    用户添加歌曲到歌单事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否添加歌曲到歌单成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_add_music_to_collection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
	write_message["origin"] = "add music to collection";
	write_message["time"] = Get_Time();
	bool ret;
	if (read_message["username"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_add_music_to_collection]: 未找到必要字段\"username\"" << endl;
		write_message["type"] = "err";
		return false;
	}

	if (read_message["collection id"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_add_music_to_collection]: 未找到必要字段\"collection id\"" << endl;
		write_message["type"] = "err";
		return false;
	}

    if (read_message["music id"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_add_music_to_collection]: 未找到必要字段\"music id\"" << endl;
		write_message["type"] = "err";
		return false;
	}

	int add_ret = db.add_music_to_collection(read_message["collection id"], read_message["music id"],read_message["username"],read_message["session"]);
	switch (add_ret)
	{
	case -1:
		logwriter.fout << Get_Time() << " [ServFunc_add_music_to_collection]: 数据库连接失败" << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	case 1:
		logwriter.fout << Get_Time() << " [ServFunc_add_music_to_collection]: 添加歌曲到歌单失败 " << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	default://0
		logwriter.fout << Get_Time() << " [ServFunc_add_music_to_collection]: 添加歌曲到歌单成功 " << read_message["username"] << "," << read_message["collection id"]<< ","<<read_message["music id"]<< endl;
		write_message["type"] = "success";
		ret = true;
	}
	return ret;
}


/*
* 函数:    bool ServFunc_remove_music_from_collection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1753935-夏子寒
* 功能:    用户从歌单中删除歌曲事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  用户是否从歌单中删除歌曲成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_remove_music_from_collection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "remove music from collection";
	write_message["time"] = Get_Time();
	bool ret;
	if (read_message["username"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_remove_music_from_collection]: 未找到必要字段\"username\"" << endl;
		write_message["type"] = "err";
		return false;
	}

	if (read_message["collection id"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_remove_music_from_collection]: 未找到必要字段\"collection id\"" << endl;
		write_message["type"] = "err";
		return false;
	}

    if (read_message["music id"] == nullptr)
	{
		logwriter.fout << Get_Time() << " [ServFunc_remove_music_from_collection]: 未找到必要字段\"music id\"" << endl;
		write_message["type"] = "err";
		return false;
	}

	int del_ret = db.remove_music_from_collection(read_message["collection id"],read_message["music id"],read_message["username"],read_message["session"]);
	switch (del_ret)
	{
	case -1:
		logwriter.fout << Get_Time() << " [ServFunc_remove_music_from_collection]: 数据库连接失败" << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	case 1:
		logwriter.fout << Get_Time() << " [ServFunc_remove_music_from_collection]: 从歌单中删除歌曲失败 " << endl;
		write_message["type"] = "err";
		ret = false;
		break;

	default://0
		logwriter.fout << Get_Time() << " [ServFunc_remove_music_from_collection]: 从歌单中删除歌曲成功 " << read_message["username"] << "," << read_message["collection id"] << "," << read_message["music id"] << endl;
		write_message["type"] = "success";
		ret = true;
	}
	return ret;
}


/*
 * 函数:    bool ServFunc_SearchMusic(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
 * 作者:    1751763-陈威
 * 功能:    搜索歌曲用的函数，返回歌曲信息列表.
 * 参数:
    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
 * 返回值:  是否搜索到该歌曲(true:成功;false:出错)
 * 备注:    
 */

bool ServFunc_SearchMusic(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "search music";
    if( read_message["content"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_SearchMusic]: 未找到必要字段\"content\"" << endl;
        write_message["type"] = "err";
        return false;
    }
	
	MESSAGE one_result;
	vector<MESSAGE> results;
	vector<string> albums,artists,links,mid,source, names;
	vector<int> music_ids;
        vector<int> song_ids;
    int search_ret=db.SearchMusic(read_message["content"],albums,artists,music_ids,links,mid,source, song_ids, names);
	int info_len=music_ids.size();
    switch( search_ret )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_SearchMusic]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
            break;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_SearchMusic]: 获取歌曲信息成功" << read_message["content"] << endl;
            write_message["type"] = "success";
			for (int i=0;i<info_len;i++)
			{
				one_result["album"]=albums[i];
				one_result["artist"]=artists[i];
				one_result["music id"]=music_ids[i];
                one_result["music name"]=names[i];
				one_result["link"]=links[i];
                one_result["mid"]=mid[i];
                one_result["source"]=source[i];
                one_result["song id"]=song_ids[i];
				results.push_back(one_result);
			}
			write_message["result"]=results;
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_SearchMusic]: 获取歌曲信息失败 " << read_message["content"] << endl;
            write_message["type"] = "empty";
            return false;
    }
    return true;
}

/*
 * 函数:    bool ServFunc_SearchCollection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
 * 作者:    1751763-陈威
 * 功能:    搜索歌单用的函数，返回歌单信息列表.
 * 参数:
    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
 * 返回值:  是否搜索到该歌单(true:成功;false:出错)
 * 备注:    
 */

bool ServFunc_SearchCollection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "search collection";
    if( read_message["content"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_SearchCollection]: 未找到必要字段\"content\"" << endl;
        write_message["type"] = "err";
        return false;
    }
	
	MESSAGE one_result;
	vector<MESSAGE> results;
	vector<string> collection_names;
	vector<int> collection_ids;
    int search_ret=db.SearchCollection(read_message["content"],collection_names,collection_ids);
	int info_len=collection_ids.size();
    switch( search_ret )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_SearchCollection]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
            break;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_SearchCollection]: 获取歌单信息成功" << read_message["content"] << endl;
            write_message["type"] = "success";
			for (int i=0;i<info_len;i++)
			{
				one_result["collection id"]=collection_ids[i];
				one_result["collection name"]=collection_names[i];
				results.push_back(one_result);
			}
			write_message["result"]=results;
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_SearchCollection]: 获取歌单信息失败 " << read_message["content"] << endl;
            write_message["type"] = "empty";
            return false;
    }
    return true;
}

/*
 * 函数:    bool ServFunc_FetchUserCollection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
 * 作者:    1751763-陈威
 * 功能:    返回用户的歌单信息列表.
 * 参数:
    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
 * 返回值:  是否搜索到用户歌单(true:成功;false:出错)
 * 备注:    
 */

bool ServFunc_FetchUserCollection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "user collection";
    if( read_message["username"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 未找到必要字段\"username\"" << endl;
        write_message["type"] = "err";
        return false;
    }
	if( read_message["session"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 未找到必要字段\"session\"" << endl;
        write_message["type"] = "err";
        return false;
    }
	int user_id=db.Session(read_message["session"]);
	switch(user_id)
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 认证失败 " << read_message["session"] << endl;
            write_message["type"] = "err";
            return false;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 认证成功 " << read_message["session"] << " ，用户id:" << user_id << endl;
    }
	int checkuser=db.CheckUser(read_message["username"],user_id);
	switch(checkuser)
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
        case 1:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 用户名与id不匹配 " << read_message["username"] << " ，用户id:" << user_id << endl;
            write_message["type"] = "err";
            return false;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 用户名与id匹配成功 " << read_message["username"] << " ，用户id:" << user_id << endl;
    }
	MESSAGE one_result;
	vector<MESSAGE> results;
	vector<string> collection_names;
	vector<int> collection_ids;
	vector<bool> owned_by_me;
    int search_ret=db.FetchUserCollection(user_id,collection_names,collection_ids,owned_by_me);
	int info_len=collection_ids.size();
    switch( search_ret )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
            break;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 获取歌单信息成功" << read_message["username"] << " ，用户id:" << user_id << endl;
            write_message["type"] = "success";
			for (int i=0;i<info_len;i++)
			{
				one_result["collection id"]=collection_ids[i];
				one_result["collection name"]=collection_names[i];
                one_result["owned by me"]=bool(owned_by_me[i]);
				results.push_back(one_result);
			}
			write_message["result"]=results;
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_FetchUserCollection]: 获取歌单信息失败 " << read_message["username"]<< " ，用户id:" << user_id  << endl;
            write_message["type"] = "empty";
            return false;
    }
    return true;
}


/*
 * 函数:    bool ServFunc_FetchCollectionDetail(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
 * 作者:    1751763-陈威
 * 功能:    返歌单内歌曲信息列表.
 * 参数:
    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
 * 返回值:  是否搜索到该歌单内歌曲(true:成功;false:出错)
 * 备注:    
 */

bool ServFunc_FetchCollectionDetail(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "collection detail";
    if( read_message["collection id"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_FetchCollectionDetail]: 未找到必要字段\"collection id\"" << endl;
        write_message["type"] = "err";
        return false;
    }
	
	MESSAGE one_result;
	vector<MESSAGE> results;
	vector<string> albums,artists,music_names,links,mid,source;
	vector<int> music_ids, song_ids;
    int search_ret=db.FetchCollectionDetail(read_message["collection id"],albums,artists,music_ids,music_names,links,mid,source, song_ids);
	int info_len=music_ids.size();
    switch( search_ret )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_FetchCollectionDetail]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
            break;
        case 0:
            logwriter.fout << Get_Time() << " [ServFunc_FetchCollectionDetail]: 获取歌曲信息成功" << read_message["collection id"] << endl;
            write_message["type"] = "success";
			for (int i=0;i<info_len;i++)
			{
				one_result["album"]=albums[i];
				one_result["artist"]=artists[i];
				one_result["music id"]=music_ids[i];
				one_result["music name"]=music_names[i];
				one_result["link"]=links[i];
                one_result["mid"]=mid[i];
                one_result["source"]=source[i];
		one_result["song id"]=song_ids[i];
				results.push_back(one_result);
			}
			write_message["result"]=results;
            break;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_FetchCollectionDetail]: 获取歌曲信息失败 " << read_message["collection id"] << endl;
            write_message["type"] = "empty";
            return false;
    }
    return true;
}

/*
* 函数:    bool ServFunc_CreateCollection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
* 作者:    1751739-杨君临
* 功能:    创建歌单事务处理函数
* 参数:    MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter
* 返回值:  歌单是否创建成功(true：成功；false：失败)
* 备注:
*/
bool ServFunc_CreateCollection(MESSAGE &read_message, MESSAGE &data, MESSAGE &write_message, DB &db, LOGWRITER &logwriter)
{
    write_message["origin"] = "create collection";
    write_message["time"] = Get_Time();
    
    if( read_message["username"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 未找到必要字段\"username\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    
    if( read_message["collection name"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 未找到必要字段\"collection name\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    
    if( read_message["session"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 未找到必要字段\"session\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    
    if( read_message["private"] == nullptr )
    {
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 未找到必要字段\"private\"" << endl;
        write_message["type"] = "err";
        return false;
    }
    // 会话认证
    int user_id = db.Session(read_message["session"]);
    switch( user_id )
    {
    case -1:
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 数据库连接失败" << endl;
        write_message["type"] = "err";
        return false;
        break;

    case 0:
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 会话认证失败 " << read_message["username"] << endl;
        write_message["type"] = "err";
        return false;
        break;

    default:
        logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 找到用户 " << read_message["username"] << " ，用户id为" << user_id << endl;
        //write_message["type"] = "success";
        break;
    }
    
    // 检查用户名和uid是否匹配
 	int checkuser=db.CheckUser(read_message["username"],user_id);
	switch( checkuser )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
        case 1:
            logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 用户名与id不匹配 " << read_message["username"] << " ，用户id:" << user_id << endl;
            write_message["type"] = "err";
            return false;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_CreateCollectionrCollection]: 用户名与id匹配成功 " << read_message["username"] << " ，用户id:" << user_id << endl;
            break;
    }

	// 在数据库中创建新歌单
    bool privacy;
    if (read_message["private"] == "false")
        privacy = true;
    else
        privacy = false;

    int collection_id = db.CreateCollection(user_id, read_message["collection name"], read_message["private"]);
    switch( collection_id )
    {
        case -1:
            logwriter.fout << Get_Time() << " [ServFunc_CreateCollection]: 创建歌单时数据库连接失败" << endl;
            write_message["type"] = "err";
            return false;
        default:
            logwriter.fout << Get_Time() << " [ServFunc_CreateCollectionrCollection]: 歌单创建成功，歌单名: " << read_message["collection name"] << " ，歌单id:" << collection_id <<   "隐私歌单：" << privacy << endl;
            write_message["type"] = "success";
            write_message["collection id"] = collection_id;
            break;
    }

    return true;
}


