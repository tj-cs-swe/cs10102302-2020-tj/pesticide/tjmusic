/*
 * 文件名:  Server_Interact.h
 * 作者:    1751400-张校
 * 概述:    包含套接字读写类成员函数实现的头文件
 */

#include "Server_Interact.h"

using namespace std;

SERVER_INTERACT::SERVER_INTERACT(int _sock, const char *_ip_addr, unsigned short _port, size_t _max_buf_size = SI_MAX_BUF_SIZE)
    : sock(_sock), port(_port), max_buf_size(_max_buf_size)
{
    fcntl(sock, F_SETFL, fcntl(sock, F_GETFL) | O_NONBLOCK);

    if( _ip_addr )
        memcpy(ip_addr, _ip_addr, 15);
    else
        memcpy(ip_addr, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 15);
    ip_addr[15] = 0;

    last_active_time = time(NULL);
}

SERVER_INTERACT::~SERVER_INTERACT()
{
    close(sock);
}

int SERVER_INTERACT::Message_Decode(string &str, MESSAGE &msg)
{
    msg = json::parse(str, nullptr, false);
    if( msg.is_discarded() )
        return -1;
    return str.length();
}

int SERVER_INTERACT::Message_Encode(string &str, MESSAGE &msg)
{
    str = msg.dump();
    return str.length();
}

int SERVER_INTERACT::Read_Raw_Data(char *read_buf, size_t read_buf_size)
{
    last_active_time = time(NULL);
    return read(sock, read_buf, read_buf_size);
}

int SERVER_INTERACT::Write_Raw_Data(const char *write_buf, size_t write_buf_size)
{
    return write(sock, write_buf, write_buf_size);
}

int SERVER_INTERACT::Read_Message(MESSAGE &msg)
{
    last_active_time = time(NULL);

    int read_len;
    char read_buf[SI_BUF_SIZE];
    while( (read_len = read(sock, read_buf, SI_BUF_SIZE)) > 0 )
    {
        read_buf[read_len] = 0;
        buf.append(read_buf, 0, read_len);
        if( buf.length() > max_buf_size)
            return 0;
    }
    if( read_len == 0 )
        return 0;
    
    int message_length = Message_Decode(buf, msg);
    if( message_length == -1 )
        return -1;
    if( message_length > buf.length() )
        return 0;
    buf.erase(0, message_length);
    return message_length;
}

int SERVER_INTERACT::Write_Message(MESSAGE &msg)
{
    string write_buf;
    if( Message_Encode(write_buf, msg) > 0 )
        return write(sock, write_buf.c_str(), write_buf.length());
    return -1;
}

int             SERVER_INTERACT::Sock()             { return sock;              }
const char *    SERVER_INTERACT::Ip_Addr()          { return ip_addr;           }
unsigned short  SERVER_INTERACT::Port()             { return port;              }
size_t          SERVER_INTERACT::Last_Active_Time() { return last_active_time;  }
