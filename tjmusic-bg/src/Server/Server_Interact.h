/*
 * 文件名:  Server_Interact.h
 * 作者:    1751400-张校
 * 概述:    包含套接字读写类 SERVER_INTERACT 和数据类型 MESSAGE 的头文件
 */

#ifndef _SERVER_INTERACT_H
#define _SERVER_INTERACT_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <map>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/prctl.h>

#include "json.hpp"

using json = nlohmann::json;

#define SI_BUF_SIZE 1024
#define SI_MAX_BUF_SIZE 1024000

// typedef std::map<std::string, std::string> MESSAGE;
typedef json MESSAGE;

class SERVER_INTERACT
{
private:
    int sock;
    char ip_addr[16];
    unsigned short port;

    std::string buf;

    int64_t last_active_time;
    size_t max_buf_size;

    // 将 string 转为 MESSAGE
    int Message_Decode(std::string &, MESSAGE &);
    // 将 MESSAGE 转为 string
    int Message_Encode(std::string &, MESSAGE &);

public:
    SERVER_INTERACT(int, const char *, unsigned short, size_t);
    ~SERVER_INTERACT();

    // 如果处理完一个事务不断开连接，可以将需要的数据保存于此
    MESSAGE data;
    
    // 获取套接字信息
    int Sock();
    // 获取IP地址信息
    const char *Ip_Addr();
    // 获取断开信息
    unsigned short Port();
    // 获取最后活动时间
    size_t Last_Active_Time();

    // 相当于read函数
    int Read_Raw_Data(char *, size_t);
    // 相当于write函数
    int Write_Raw_Data(const char *, size_t);

    // 读取数据并转化为MESSAGE
    int Read_Message(MESSAGE &);
    // 将MESSAGE转化为字符串并写入套接字
    int Write_Message(MESSAGE &);
};

#endif