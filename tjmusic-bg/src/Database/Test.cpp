/*
 * 文件名:  Test.cpp
 * 作者:    1751400-张校
 * 概述:    可单独测试Database
 */


#include <iostream>
#include <string>
#include "Database.h"

using namespace std;

int main()
{
    string user_name = "testtest";
    string password = "123456";
    string session_code;

    DB db("127.0.0.1", "root", "root123", "tjmusic", "utf8");
    if( db.Connect() )
    {
        cout << "Register: " << db.Register(user_name, password) << endl;
        cout << "Login: " << db.Login(user_name, password, session_code) << endl;
        cout << "session_code: " << session_code << endl;
        cout << "SignOut: " << db.SignOut("18") << endl;
        db.Disconnect();
    }

    return 0;
}


// int main()//已测试删除存在/不存在用户两种情况
// {
//     string user_name;
//     cout<<"input username"<<endl;
//     cin >> user_name;

//     DB db("127.0.0.1", "root", "123456", "tjmusic", "utf8");
//     if( db.Connect() )
//     {
//         cout << (db.example_function(user_name) ? "user exists" : "user doesnt exist") << endl;
//         cout << (db.CancelAccount(user_name)==0 ? "cancel account ok" : "cancel account error") << endl;
//         cout << (db.example_function(user_name) ? "user exists" : "user doesnt exist") << endl;
//         cout << (db.CancelAccount(user_name)==0 ? "cancel account ok" : "cancel account error") << endl;
//         db.Disconnect();

//     }
//     cout<<"test finished"<<endl;
//     return 0;
// }
