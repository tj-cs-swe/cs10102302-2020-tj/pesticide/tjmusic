/*
 * 文件名:  Database.h
 * 作者:    1751400-张校
 * 概述:    存放数据库类DB的定义及相关函数
 */
#ifndef _DATABASE_H
#define _DATABASE_H

#include <iostream>
#include <string>
#include <mysql.h>
#include <cstdlib>
#include <cstring>
#include <map>
#include <random>
#include <vector>
class DB
{
private:
    std::string DB_host, DB_username, DB_password, DB_dbname, DB_charset;
    MYSQL *mysql;
    bool long_term_connection;
    
    // 连接数据库
    bool _Connect();
    // 断开连接
    void _Disconnect();
public:
    /* 通用成员函数，请勿修改 */
    // 构造函数
    DB(std::string, std::string, std::string, std::string, std::string);
    // 连接数据库
    bool Connect();
    // 断开连接
    void Disconnect();

    // 以下函数不推荐使用，需要执行sql语句则定义一般成员函数
    bool Query(std::string);
    MYSQL_RES *Store_Result();
    void Free_Result(MYSQL_RES *);

    /* 一般成员函数 */

    // 样例函数：查询用户是否存在
    int example_function(std::string);
    int Login(std::string, std::string, std::string&);
    int Session(std::string);
    int CheckUser(std::string,int);
    //注销用户
    int CancelAccount(std::string,int);
    //用户登出
    int SignOut(std::string);
    //用户注册
	int Register(std::string, std::string);
    //用户搜索歌曲
    int SearchMusic(std::string,std::vector<std::string> &,std::vector<std::string> &,std::vector<int> &,std::vector<std::string> &,std::vector<std::string> &,std::vector<std::string> &, std::vector<int> &, std::vector<std::string> &);
    //用户搜索歌单
    int SearchCollection(std::string,std::vector<std::string> &,std::vector<int> &);
    //查看用户歌单
    int FetchUserCollection(int,std::vector<std::string> &,std::vector<int> &,std::vector<bool> &);
    //查看歌单内歌曲
    int FetchCollectionDetail(int,std::vector<std::string> &,std::vector<std::string> &,std::vector<int> &,std::vector<std::string> &,std::vector<std::string> &,std::vector<std::string> &,std::vector<std::string> &, std::vector<int> &);
	//用户收藏歌单
	int add_user_collections(int, std::string, std::string);
	//用户删除歌单
	int del_user_collections(int, std::string, std::string);
    //添加歌曲到用户歌单
    int add_music_to_collection(int collection_id,int music_id,std::string username, std::string session);
    //从用户歌单删除歌曲
    int remove_music_from_collection(int collection_id,int music_id, std::string username,std::string session);
    //用户创建歌单
    int CreateCollection(int, std::string, bool);
};

#endif
