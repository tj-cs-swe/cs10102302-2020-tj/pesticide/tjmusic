/*
 * 文件名:  Database.cpp
 * 作者:    1751400-张校
 * 概述:    存放数据库类DB的成员函数定义和工具函数定义
 */
#include "Database.h"

using namespace std;

/*
 * 函数:
 * 作者:
 * 功能:
 * 参数:
 * 返回值:
 * 备注:
 */


/*****************************************************************************
 *                                 工具函数                                   *
 *****************************************************************************/

/*
 * 函数:    string To_Date(time_t sec)
 * 作者:    1751400-张校
 * 功能:    将描述转换为string表示的日期
 * 参数:
    time_t  sec:    秒数
 * 返回值:  转换日期的string表示
 * 备注:
 */
static string To_Date(time_t sec = time(NULL))
{
    char buf[20];
    struct tm *ttime;
    ttime = localtime(&sec);
    strftime(buf, 20, "%Y-%m-%d %H:%M:%S", ttime);
    return string(buf);
}



/*
 * 函数:    V_To_String(int64_t v)
 * 作者:    1751400-张校
 * 功能:    将整数转换为string表示
 * 参数:
    int64_t v:  待转换整数
 * 返回值:  转换后整数的string表示
 * 备注:    由于部分c++版本不支持to_string函数，因此定义此函数
 */
static string V_To_String(int64_t v)
{
    if( v == 0 )
        return string(1, '0');
    bool negative = false;
    if( v < 0 )
    {
        negative = true;
        v *= -1;
    }

    int a[100] = {0}, i = 0;
    while( v )
    {
        a[i ++] = v % 10;
        v /= 10;
    }

    string b;
    if( negative )
        b.append(1, '-');
    while( i -- )
    {
        b.append(1, (char)(a[i] + '0'));
    }
    return b;
}


/*
 * 函数:    Gen_Rand_Str(const unsigned len)
 * 作者:    1751739-杨君临
 * 功能:    生成指定长度的随机字符串，包含数字和大小写字母
 * 参数:
 *  const unsigned len:  返回的字符串的长度
 * 返回值:  生成的随机字符串
 * 备注:
 */
static string Gen_Rand_Str(const unsigned len)
{
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    static default_random_engine e(time(NULL));
    static uniform_int_distribution<unsigned> u(0, sizeof(alphanum) - 2);
    string rand_str;
    rand_str.resize(len);

    for (auto &c : rand_str)
        c = alphanum[u(e)];

    return rand_str;
}

/* 在这里定义需要的工具函数(static 函数) */




/*****************************************************************************
 *                               通用成员函数                                 *
 *****************************************************************************/



/*
 * 函数:    DB::DB(string _DB_host, string _DB_username, string _DB_password, string _DB_dbname, string _DB_charset)
 * 作者:    1751400-张校
 * 功能:    构造DB对象
 * 参数:
    string  _DB_host:       数据库主机地址
    string  _DB_username:   登录数据库用户名
    string  _DB_password:   登录数据库密码
    string  _DB_dbname:     使用的数据库名
    string  _DB_charset:    数据库字符编码
 * 返回值:
 * 备注:    数据库对象创建后不可修改
 */
DB::DB(string _DB_host, string _DB_username, string _DB_password, string _DB_dbname, string _DB_charset)
    : DB_host(_DB_host), DB_username(_DB_username), DB_password(_DB_password), DB_dbname(_DB_dbname), DB_charset(_DB_charset)
{
    mysql = NULL;
    long_term_connection = false;
}



/*
 * 函数:    DB::_Connect()
 * 作者:    1751400-张校
 * 功能:    连接数据库
 * 参数:
 * 返回值:  是否连接成功
 * 备注:    该函数为私有函数，由成员函数调用。
 */
bool DB::_Connect()
{
    if( (mysql = mysql_init(NULL)) == NULL )
    {
        cout << "init failed" << endl;
        return false;
    }

    if( mysql_real_connect(mysql, DB_host.c_str(), DB_username.c_str(), DB_password.c_str(), DB_dbname.c_str(), 0, NULL, 0) == NULL )
    {
        mysql_close(mysql);
        cout << "connect failed" << endl;
        return false;
    }
    
    mysql_set_character_set(mysql, DB_charset.c_str());
    return true;
}



/*
 * 函数:    DB::Connect()
 * 作者:    1751400-张校
 * 功能:    连接数据库
 * 参数:
 * 返回值:  是否连接成功
 * 备注:    该函数由非成员函数调用。该函数建立的连接不会由_Disconnect()函数断开
 */
bool DB::Connect()
{
    if( mysql )
        return true;
    if( _Connect() )
    {
        long_term_connection = true;
        return true;
    }
    return false;
}



/*
 * 函数:    DB::_Disconnect()
 * 作者:    1751400-张校
 * 功能:    数据库断开连接
 * 参数:
 * 返回值:
 * 备注:    该函数为私有函数，由成员函数调用。该函数不会断开由Connect()函数建立的连接
 */
void DB::_Disconnect()
{
    if( mysql && ! long_term_connection )
    {
        mysql_close(mysql);
        mysql = NULL;
    }
}



/*
 * 函数:    DB::Disconnect()
 * 作者:    1751400-张校
 * 功能:    数据库断开连接
 * 参数:
 * 返回值:
 * 备注:    该函数由非成员函数调用。用于断开Connect()函数建立的连接
 */
void DB::Disconnect()
{
    if( ! mysql )
        return;
    long_term_connection = false;
    _Disconnect();
}



/*
 * 函数:    DB::Query(string query)
 * 作者:    1751400-张校
 * 功能:    执行一个数据库语句
 * 参数:
    string  query:  执行的语句
 * 返回值:  是执行接成功
 * 备注:    不推荐使用
 */
bool DB::Query(string query)
{
    if( ! mysql && ! _Connect() )
        return false;
    bool succeeded = false;
    if( ! mysql_query(mysql, query.c_str()) )
        succeeded = true;
    _Disconnect();
    return succeeded;
}



/*
 * 函数:    MYSQL_RES *DB::Store_Result()
 * 作者:    1751400-张校
 * 功能:    获取查询结果
 * 参数:
    string  query:  执行的语句
 * 返回值:  查询结果或NULL
 * 备注:    不推荐使用
 */
MYSQL_RES *DB::Store_Result()
{
    return mysql_store_result(mysql);
}



/*
 * 函数:    DB::Free_Result(MYSQL_RES *result)
 * 作者:    1751400-张校
 * 功能:    释放查询结果
 * 参数:
    MYSQL_RES *result:  结果保存指针
 * 返回值:
 * 备注:
 */
void DB::Free_Result(MYSQL_RES *result)
{
    mysql_free_result(result);
}

/*****************************************************************************
 *                               一般成员函数                                 *
 *****************************************************************************/

/*
 * 函数:    DB::example_function(string user_name)
 * 作者:    1751400-张校
 * 功能:    查询用户是否存在
 * 参数:
    string  user_name:  用户名
 * 返回值:  用户是否存在
 * 备注:    样例函数，不保证能成功运行（视数据表结构）
 */
int DB::example_function(string user_name)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    
    int user_id = 0;
    string query;
    query.append("SELECT user_id FROM users WHERE user_name = \"").append(user_name).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                MYSQL_ROW row = mysql_fetch_row(result);
                user_id = atoi(row[0]);
            }
            mysql_free_result(result);
        }
    }

    _Disconnect(); // 断开连接
    return user_id;
}

/*
 * 函数:    DB::CancelAccount(string user_name,int user_id)
 * 作者:    1751763-陈威
 * 功能:    注销用户
 * 参数:
    string  user_name:  用户名(来自json中的user_name)
    int user_id:  用户编号(来自session中的user_id)
 * 返回值:  是否成功注销用户(0:成功;-1:执行出现错误;1:没有该用户)
 * 备注:    
 */
int DB::CancelAccount(string user_name,int user_id)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret;
    string query;
    query.append("DELETE FROM users WHERE user_name = \"").
    append(user_name).append("\" and user_id=").append(to_string(user_id)).append(";");
    if( mysql_query(mysql, query.c_str()) )//执行出现了错误
    {
        _Disconnect(); // 断开连接
        return -1;
    }
    if (mysql_affected_rows(mysql)==0)//没有删除任何一行
        ret=1;
    else 
        ret=0;  
    _Disconnect(); // 断开连接
    return ret;
}


// 以下两个函数为基本函数，需要尽早实现

/*
 * 函数: int DB::Login(string username, string password)
 * 作者: 1751739-杨君临
 * 功能: 用户登录，返回是否登录成功
 * 参数:
    string  username:       用户名
    string  password:       用户密码
    string& session_code    返回的会话id
 * 返回值:
    -1: 数据库连接失败
    0:  登录失败
    >0: 登录成功，返回用户id
 * 备注: 若成功则需要修改用户的最近登录时间并创建会话，
 *       若请求登录的账号已经登录，则注销原来的会话，建立新的会话
 */
int DB::Login(string username, string password, string &session_code)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int user_id = 0;
    
    string query;
    query.append("SELECT * FROM users WHERE user_name = \"").append(username).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                
                MYSQL_ROW row = mysql_fetch_row(result);
                // 核验密码，若密码正确则登录成功
                if ( password == string(row[2]) ) {
                    user_id = atoi(row[0]);
                    
                    //SignOut(string(row[0])); // 注销该用户已存在的会话
                    query.clear();
                    query.append("DELETE FROM sessions WHERE user_id = ").append(row[0]).append(";");
                    mysql_query(mysql, query.c_str());

                    string date = To_Date();
                    session_code = Gen_Rand_Str(32);

                    // 修改最近登陆时间
                    query.clear();
                    query.append("UPDATE users SET last_login_time = \"").append(date).append("\" WHERE user_name = \"").append(username).append("\";");
                    mysql_query(mysql, query.c_str());

                    // 创建会话
                    query.clear();
                    query.append("INSERT INTO sessions VALUES (\"").append(session_code).append("\", ").append(row[0]).append(", \"").append(date).append("\");");
                    mysql_query(mysql, query.c_str());
                }
            }
            mysql_free_result(result);
        }
    }

    _Disconnect(); // 断开连接
    return user_id;
}


/*
 * 函数: int DB::Session(string session_code)
 * 作者: 1751739-杨君临
 * 功能: 会话认证，返回是否认证成功
 * 参数:
    string  session_code:    会话id
 * 返回值:
    -1: 数据库连接失败
    0:  认证失败
    >0: 认证成功，返回用户id
 * 备注: 若成功则需要修会话最后活跃时间
 */
int DB::Session(string session_code)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int user_id = 0;

    // 查询session_id对应的user_id
    string query;
    query.append("SELECT user_id FROM sessions WHERE session_code = \"").append(session_code).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                MYSQL_ROW row = mysql_fetch_row(result);
                user_id = atoi(row[0]);

                // 更新最后活跃时间
                query.clear();
                query.append("UPDATE sessions SET last_active_time = \"").append(To_Date()).append("\" WHERE session_code = \"").append(session_code).append("\"");
                mysql_query(mysql, query.c_str());
            }
            mysql_free_result(result);
        }
    }

    _Disconnect(); // 断开连接
    return user_id;
}


/*
 * 函数:    DB::SignOut(string user_name)
 * 作者:    1753935-夏子寒
 * 功能:    注销用户
 * 参数:
    string  user_name:  用户名
 * 返回值:  是否成功登出(0:成功;-1:执行出现错误;1:没有该用户)
 * 备注:    
 */
int DB::SignOut(string user_id)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret;
    string query;
    query.append("DELETE FROM sessions WHERE user_id = ").append(user_id).append(";");
    if( mysql_query(mysql, query.c_str()) )//执行出现了错误
    {
        _Disconnect(); // 断开连接
        return -1;
    }
    if (mysql_affected_rows(mysql)==0)//没有删除任何一行
        ret=1;
    else 
        ret=0;  
    _Disconnect(); // 断开连接
    return ret;
}


/*
* 函数:    DB::Register(string user_name，string password)
* 作者:    1753917-谷贤明
* 功能:    用户注册
* 参数:
string  user_name:  用户名
string  password:  用户密码
* 返回值:  用户是否注册成功(0:成功;-1:执行出现错误;1:注册失败)
* 备注:  
*/
int DB::Register(string user_name,string password)
{
	if (!mysql && !_Connect()) // 连接
		return -1;
    int ret;
    string dif_name;//检测用户名是否相同
	dif_name.append("SELECT * FROM users WHERE user_name = \"").append(user_name).append("\";");
	if (!mysql_query(mysql, dif_name.c_str()))
	{
        MYSQL_RES *result = mysql_store_result(mysql);
        if (result)
        {
            if (mysql_num_rows(result) >= 1)
                return 1;
        mysql_free_result(result);
        }
	}
	
	string query;
	query.append("INSERT INTO users (user_name, user_password, last_login_time) VALUES (\"").append(user_name).append("\", \"").append(password).append("\", \"1970-01-01 00:00:00\");");
	if (!mysql_query(mysql, query.c_str()))
	{
		if (mysql_affected_rows(mysql) == 1)
			ret = 0;
		else 
			ret = 1;
	}
	else//执行出现错误
	{
		ret = -1;
	}

	_Disconnect(); // 断开连接
	return ret;
}

/*
* 函数:    DB::add_user_collections(string collection_id, string username, string session)
* 作者:    1753917-谷贤明
* 功能:    用户收藏歌单
* 参数:
string username:      用户名
string collection_id: 歌单ID
string session:       会话认证
* 返回值:  收藏成功返回0，失败返回1，程序错误返回-1
* 备注:    
*/
int DB::add_user_collections(int collection_id, string username, string session)
{
	if (!mysql && !_Connect()) // 连接
		return -1;
	int ret, user_id;

	string query;
	query.append("SELECT * FROM users WHERE user_name = \"").append(username).append("\";");
	if (!mysql_query(mysql, query.c_str()))
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) == 0)
				return 1;//未找到用户名
            mysql_free_result(result);
		}
	}

    // 会话认证
    query.clear();
    query.append("SELECT user_id FROM sessions WHERE session_code = \"").append(session).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                MYSQL_ROW row = mysql_fetch_row(result);
                user_id = atoi(row[0]);

                // 更新最后活跃时间
                query.clear();
                query.append("UPDATE sessions SET last_active_time = \"").append(To_Date()).append("\" WHERE session_code = \"").append(session).append("\"");
                mysql_query(mysql, query.c_str());
            }
            else
            {
                return 1;
            }
            mysql_free_result(result);
        }
    }

	query.clear();//检测用户还未收藏,即歌单不存在用户收藏的歌单中
	query.append("SELECT * FROM user_collections WHERE user_id = ").append(V_To_String(user_id)).append(" and collection_id = ").append(V_To_String(collection_id)).append(";");
	if ( !mysql_query(mysql, query.c_str()) )
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) >= 1)
				return 1;
            mysql_free_result(result);
		}
	}

	query.clear();
	query.append("INSERT INTO user_collections (user_id, collection_id) VALUES (").append(V_To_String(user_id)).append(", ").append(V_To_String(collection_id)).append(");");
	if (!mysql_query(mysql, query.c_str()))
	{
		if (mysql_affected_rows(mysql) == 1)
			ret = 0;
		else
			ret = 1;
	}
	else//执行出现错误
	{
		ret = -1;
	}

	_Disconnect(); // 断开连接
	return ret;
}


/*
* 函数:    DB::del_user_collections(int collection_id, string username, string session)
* 作者:    1753917-谷贤明
* 功能:    用户删除歌单
* 参数:
string username:       用户名
string collection_id:  歌单ID
string session:        会话认证
* 返回值:  删除成功返回0，失败返回1，程序错误返回-1
* 备注:
*/
int DB::del_user_collections(int collection_id, string username, string session)
{
	if (!mysql && !_Connect()) // 连接
		return -1;
	int ret, uid;

    string query;
	query.append("SELECT * FROM users WHERE user_name = \"").append(username).append("\";");
	if (!mysql_query(mysql, query.c_str()))
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) == 0)
				return 1;//未找到用户名
            mysql_free_result(result);
		}
	}

    // 会话认证
    // uid = Session(session);
    // if ( uid <= 0 )
    //     return 1;
    query.clear();
    query.append("SELECT user_id FROM sessions WHERE session_code = \"").append(session).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                MYSQL_ROW row = mysql_fetch_row(result);
                uid = atoi(row[0]);

                // 更新最后活跃时间
                query.clear();
                query.append("UPDATE sessions SET last_active_time = \"").append(To_Date()).append("\" WHERE session_code = \"").append(session).append("\"");
                mysql_query(mysql, query.c_str());
            }
            else
            {
                return 1;
            }
            mysql_free_result(result);
        }
    }

	// string exist;//检测用户已经收藏,即歌单已经存在用户收藏的歌单中
	// exist.append("SELECT * FROM user_collections WHERE user_id = ").append(V_To_String(uid)).append("and collection_id = ").append(V_To_String(collection_id)).append(";");
	// if (!mysql_query(mysql, exist.c_str()))
	// {
	// 	MYSQL_RES *result = mysql_store_result(mysql);
	// 	if (result)
	// 	{
	// 		if (mysql_num_rows(result) == 0)
	// 			return 1;
    //         mysql_free_result(result);
	// 	}
	// }

	
    // 删除歌单
	query.clear();
	query.append("DELETE FROM user_collections WHERE user_id = ").append(V_To_String(uid)).append(" and collection_id = ").append(V_To_String(collection_id)).append(";");
	if ( !mysql_query(mysql, query.c_str()) )
	{
		if (mysql_affected_rows(mysql) == 1)
			ret = 0;
		else
			ret = 1;
	}
	else//执行出现错误
	{
		return -1;
	}

    // 判断用户是否是歌单的所有者
    bool flag = false;
    query.clear();
    query.append("SELECT * FROM music_collections WHERE collection_id = ").append(V_To_String(collection_id)).append(" and owner_user = ").append(V_To_String(uid)).append(";");
    if ( !mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) >= 1 )
            {
                flag = true;
            }
            mysql_free_result(result);
        }

    }

    // 若用户是歌单所有者，删除 music_collection 和 music_in_collection 中的相关内容
    if ( flag )
    {
        query.clear();
        query.append("DELETE FROM music_in_collection WHERE collection_id = ").append(V_To_String(collection_id)).append(";");
        if ( mysql_query(mysql, query.c_str()) )
        {
            return -1;
        }

        query.clear();
        query.append("DELETE FROM user_collections WHERE collection_id = ").append(V_To_String(collection_id)).append(";");
        if ( mysql_query(mysql, query.c_str()) )
        {
            return -1;
        }

        query.clear();
        query.append("DELETE FROM music_collections WHERE collection_id = ").append(V_To_String(collection_id)).append(";");
        if ( mysql_query(mysql, query.c_str()) )
        {
            return -1;
        }


    }

	_Disconnect(); // 断开连接
	return ret;
}

/*
* 函数:    DB::add_music_to_collection(int collection_id,int music_id, string session)
* 作者:    1753935-夏子寒
* 功能:    用户添加歌曲歌单
* 参数:
string  user_name:     用户ID
string collection_id:  歌单ID
string music_id:       歌曲ID
* 返回值:  添加成功返回0，失败返回1，程序错误返回-1
* 备注:    
*/
int DB::add_music_to_collection(int collection_id,int music_id,string username, string session)
{
	if (!mysql && !_Connect()) // 连接
		return -1;
	int ret;

    string user_id;
	user_id.append("SELECT * FROM users WHERE user_name = \"").append(username).append("\";");
	if (!mysql_query(mysql, user_id.c_str()))
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) == 0)
            {
                mysql_free_result(result);
				return 1;//未找到用户名
            }
            mysql_free_result(result);
		}
	}

    // 会话认证
    string query;
    query.append("SELECT user_id FROM sessions WHERE session_code = \"").append(session).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                MYSQL_ROW row = mysql_fetch_row(result);

                // 更新最后活跃时间
                query.clear();
                query.append("UPDATE sessions SET last_active_time = \"").append(To_Date()).append("\" WHERE session_code = \"").append(session).append("\"");
                mysql_query(mysql, query.c_str());
            }
            else
            {
                return 1;
            }
            mysql_free_result(result);
        }
    }

	string no_exist;//检测用户歌单中是否有此歌曲
	no_exist.append("SELECT * FROM music_in_collection WHERE collection_id = ").append(V_To_String(collection_id)).append(" and music_id = ").append(V_To_String(music_id)).append(";");
	if (!mysql_query(mysql, no_exist.c_str()))
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) >= 1) 
            {
                mysql_free_result(result);
				return 1;            
            }
            mysql_free_result(result);
		}
	}

	query.clear();
	query.append("INSERT INTO music_in_collection (collection_id, music_id) VALUES (").append(V_To_String(collection_id)).append(", ").append(V_To_String(music_id)).append(");");
	if (!mysql_query(mysql, query.c_str()))
	{
		if (mysql_affected_rows(mysql) == 1)
			ret = 0;
		else
			ret = 1;
	}
	else//执行出现错误
	{
		ret = -1;
	}

	_Disconnect(); // 断开连接
	return ret;
}

/*
* 函数:    DB::remove_music_from_collection(int collection_id,int music_id, string session)
* 作者:    1753935-夏子寒
* 功能:    用户从歌单中删除歌曲
* 参数:
string  user_name:     用户ID
string collection_id:  歌单ID
string music_id:       歌曲ID
* 返回值:  删除成功返回0，失败返回1，程序错误返回-1
* 备注:
*/
int DB::remove_music_from_collection(int collection_id,int music_id, string username,string session)
{
	if (!mysql && !_Connect()) // 连接
		return -1;
	int ret;

    string user_id;
	user_id.append("SELECT * FROM users WHERE user_name = \"").append(username).append("\";");
	if (!mysql_query(mysql, user_id.c_str()))
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) == 0)
            {
                mysql_free_result(result);
				return 1;//未找到用户名
            }
            mysql_free_result(result);
		}
	}

    // 会话认证
    string query;
    query.append("SELECT user_id FROM sessions WHERE session_code = \"").append(session).append("\";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) == 1 )
            {
                MYSQL_ROW row = mysql_fetch_row(result);

                // 更新最后活跃时间
                query.clear();
                query.append("UPDATE sessions SET last_active_time = \"").append(To_Date()).append("\" WHERE session_code = \"").append(session).append("\"");
                mysql_query(mysql, query.c_str());
            }
            else
            {
                return 1;
            }
            mysql_free_result(result);
        }
    }

	string no_exist;//检测用户歌单中是否有此歌曲
	no_exist.append("SELECT * FROM music_in_collection WHERE collection_id = ").append(V_To_String(collection_id)).append(" and music_id = ").append(V_To_String(music_id)).append(";");
	if (!mysql_query(mysql, no_exist.c_str()))
	{
		MYSQL_RES *result = mysql_store_result(mysql);
		if (result)
		{
			if (mysql_num_rows(result) == 0)
            {
                mysql_free_result(result);
				return 1;
            }
            mysql_free_result(result);
		}
	}
	
	query.clear();
	query.append("DELETE FROM music_in_collection WHERE collection_id = ").append(V_To_String(collection_id)).append(" and music_id = ").append(V_To_String(music_id)).append(";");
	if (!mysql_query(mysql, query.c_str()))
	{
		if (mysql_affected_rows(mysql) == 1)
			ret = 0;
		else
			ret = 1;
	}
	else//执行出现错误
	{
		ret = -1;
	}

	_Disconnect(); // 断开连接
	return ret;
}


/*
 * 函数:    int DB::SearchMusic(string song_name,vector<string> &album_names,vector<string> &artist_names,vector<int> &music_ids,vector<string> &music_links)
 * 作者:    1751763-陈威
 * 功能:    搜索音乐
 * 参数:    song_name   :乐曲名称
 *          album_names :专辑名称列表
 *          artist_names:歌手名称列表
 *          music_links :链接列表
 * 返回值:  是否成功搜索到歌曲(0:搜索到了;-1:连接数据库失败;1:没有搜索到;2:执行搜索语句出错)
 * 备注:    
 */
int DB::SearchMusic(string song_name,vector<string> &album_names,vector<string> &artist_names,vector<int> &music_ids,vector<string> &music_links,vector<string> &music_mid,vector<string> &music_source, vector<int> &song_ids, vector<string> &music_name)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret;
    string query;
    query.append("SELECT music_id,singers,album_name,music_link,song_mid,song_source, song_id, music_name FROM musics WHERE music_name like '").append(song_name).append("%';");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if (mysql_num_rows(result) >= 1)
            ret=0;
        else
            ret=1;
        if( result )
        {
            MYSQL_ROW row;
            while ((row = mysql_fetch_row(result)) != NULL)
            {
                music_ids.push_back(atoi(row[0]));
                artist_names.push_back(row[1]);
                album_names.push_back(row[2]);
                music_links.push_back(row[3]);
                music_mid.push_back(row[4]);
                music_source.push_back(row[5]);
                song_ids.push_back(atoi(row[6]));
                music_name.push_back(row[7]);
            }
            mysql_free_result(result);
        }
        
    }
    else
        ret=2;
    _Disconnect(); // 断开连接
    return ret;
}


/*
 * 函数:    int DB::SearchCollection(string keyword,vector<string> &collection_names,vector<int> &collection_ids)
 * 作者:    1751763-陈威
 * 功能:    搜索歌单
 * 参数:    keyword ：歌单关键词
 *          collection_names: 歌单名称列表
 *          collection_ids   :歌单编号列表
 * 
 * 返回值:  是否成功搜索到歌单(0:搜索到了;-1:连接数据库失败;1:没有搜索到;2:执行搜索语句出错)
 * 备注:    
 */
int DB::SearchCollection(string keyword,vector<string> &collection_names,vector<int> &collection_ids)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret;
    string query;
    query.append("SELECT collection_id,collection_name FROM music_collections WHERE collection_name like \"").append(keyword).append("%\" and private='false';");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if (mysql_num_rows(result) >= 1)
            ret=0;
        else
            ret=1;
        if( result )
        {
            MYSQL_ROW row;
            while ((row = mysql_fetch_row(result)) != NULL)
            {
                collection_ids.push_back(atoi(row[0]));
                collection_names.push_back(row[1]);
            }
            mysql_free_result(result);
        }
        
    }
    else
        ret=2;
    
    _Disconnect(); // 断开连接
    return ret;
}

/*
 * 函数:    int DB::FetchUserCollection(int user_id,vector<string> &collection_names,vector<int> &collection_ids, vector<bool> &owned_by_me)
 * 作者:    1751763-陈威
 * 功能:    搜索歌单
 * 参数:    user_id ：用户编号
 *          collection_names: 歌单名称列表
 *          collection_ids   :歌单编号列表
 *          owned_by_me     : 歌单是否由该用户创建（拥有）
 * 
 * 返回值:  是否成功搜索到歌单(0:搜索到了;-1:连接数据库失败;1:没有搜索到;2:执行搜索语句出错)
 * 备注:    
 */
int DB::FetchUserCollection(int user_id,vector<string> &collection_names,vector<int> &collection_ids, vector<bool> &owned_by_me)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret;
    string query;
    query.append("SELECT b.collection_id,b.collection_name,b.owner_user FROM user_collections AS a,music_collections AS b WHERE a.user_id = ").
    append(to_string(user_id)).append(" and a.collection_id=b.collection_id;");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if (mysql_num_rows(result) >= 1)
            ret=0;
        else
            ret=1;
        if( result )
        {
            MYSQL_ROW row;
            while ((row = mysql_fetch_row(result)) != NULL)
            {
                collection_ids.push_back(atoi(row[0]));
                collection_names.push_back(row[1]);
                owned_by_me.push_back(user_id == atoi(row[2]));
            }
            mysql_free_result(result);
        }
        
    }
    else
        ret=2;
    
    _Disconnect(); // 断开连接
    return ret;
}


/*
 * 函数:    int DB::FetchCollectionDetail(int collection_id,vector<string> &album_names,vector<string> &artist_names,vector<int> &music_ids,vector<string> &music_names,vector<string> &music_links)
 * 作者:    1751763-陈威
 * 功能:    搜索音乐
 * 参数:    collection_id   :歌单编号
 *          album_names     :专辑名称列表
 *          artist_names    :歌手名称列表
 *          music_ids       :音乐编号列表
 *          music_names     :音乐名称列表
 *          music_links     :链接列表
 * 返回值:  是否成功搜索到歌曲(0:搜索到了;-1:连接数据库失败;1:没有搜索到;2:执行搜索语句出错)
 * 备注:    
 */
int DB::FetchCollectionDetail(int collection_id,vector<string> &album_names,vector<string> &artist_names,vector<int> &music_ids,vector<string> &music_names,vector<string> &music_links,vector<string> &music_mid,vector<string> &music_source, vector<int> &song_ids)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret;
    string query;
    query.append("SELECT a.music_id,a.singers,a.album_name,a.music_name,a.music_link,a.song_mid,a.song_source,a.song_id FROM musics AS a ,music_in_collection as b WHERE b.collection_id= ")
    .append(to_string(collection_id)).append(" and a.music_id=b.music_id;");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if (mysql_num_rows(result) >= 1)
            ret=0;
        else
            ret=1;
        if( result )
        {
            MYSQL_ROW row;
            while ((row = mysql_fetch_row(result)) != NULL)
            {
                music_ids.push_back(atoi(row[0]));
                artist_names.push_back(row[1]);
                album_names.push_back(row[2]);
                music_names.push_back(row[3]);
                music_links.push_back(row[4]);
                music_mid.push_back(row[5]);
                music_source.push_back(row[6]);
                song_ids.push_back(atoi(row[7]));
            }
            mysql_free_result(result);
        }
        
    }
    else
        ret=2;
    _Disconnect(); // 断开连接
    return ret;
}

/*
 * 函数: int DB::CheckUser(string user_name,int user_id)
 * 作者: 1751763-陈威
 * 功能: 判断用户名和uid是否匹配
 * 参数:
    sting user_name    用户名
    int user_id 用户id
 * 返回值:
    -1: 数据库连接失败
    0:  匹配成功
    >0: 匹配失败
 * 
 */
int DB::CheckUser(string user_name,int user_id)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret = 1;

    string query;
    query.append("SELECT * FROM users WHERE user_name = \"").append(user_name).append("\" and user_id=").append(to_string(user_id)).append(";");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        MYSQL_RES *result = mysql_store_result(mysql);
        if( result )
        {
            if( mysql_num_rows(result) >= 1 )
                ret=0;
            mysql_free_result(result);
        }
    }
    _Disconnect(); // 断开连接
    return ret;
}

/*
 * 函数: int DB::CreateCollection(int user_id, string collection_name, bool private)
 * 作者: 1751739-杨君临
 * 功能: 在数据库中创建新歌单
 * 参数:
    int     user_id         用户id
    string  collection_name 歌单名
    bool    private         歌单是否为隐私歌单
 * 返回值:
    -1: 数据库连接失败
    >=0: 创建成功，返回歌单id
 * 
 */
int DB::CreateCollection(int user_id, string collection_name, bool privacy)
{
    if( ! mysql && ! _Connect() ) // 连接
        return -1;
    int ret = -1;

    string pri;
    if (privacy)
        pri = "true";
    else
        pri = "false";

    string query;
    query.append("INSERT INTO music_collections (collection_name, owner_user, private) VALUES (\"").append(collection_name).append("\", ").append(to_string(user_id)).append(", \"").append(pri).append("\");");
    if( ! mysql_query(mysql, query.c_str()) )
    {
        
	if ( mysql_affected_rows(mysql) == 1 ) 
        {
            // 添加成功，获取歌单id
            query.clear();
            query = "SELECT LAST_INSERT_ID();";
            if( ! mysql_query(mysql, query.c_str()) ) 
            {
                MYSQL_RES *result = mysql_store_result(mysql);
                if( result )
                {
                    MYSQL_ROW row = mysql_fetch_row(result);
                    ret = atoi(row[0]);
                    query.clear();
                    query.append("INSERT INTO user_collections (user_id, collection_id) VALUES (").append(to_string(user_id)).append(", ").append(to_string(ret)).append(");");
                    mysql_query(mysql, query.c_str());
                    mysql_free_result(result);
                }
            }
        }
    }
    _Disconnect(); // 断开连接
    return ret;
}
