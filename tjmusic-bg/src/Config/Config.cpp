/*
 * 文件名:  Config.cpp
 * 作者:    1751400-张校
 * 概述:    存放配置文件类CONFIG的成员函数定义
 */

#include "Config.h"

using namespace std;

int CONFIG::Add_Config_By_File(const char *filename)
{
    fstream fin(filename, ios::in);
    if( ! fin )
        return 0;
    int n = 0;
    while( ! fin.eof() )
    {
        char buf[CONF_BUF_SIZE];
        fin.getline(buf, CONF_BUF_SIZE);
        if( !buf[0] || buf[0] == '#' || buf[0] == '\r' || buf[0] == '\n' )
            continue;
        char *head = buf, *p = buf;
        while( *p && *p != '=' )
            p ++;
        string key(head, p - head);
        head = p + 1;
        p = head;
        while( *p && *p != '\r' && *p != '\r' )
            p ++;
        string value(head, p - head);
        items[key] = value;
        n ++;
    }
    fin.close();
    return n;
}

/*
 * 函数:    void Cmd_Argument_Process(int argc, char **argv, CONFIG &config)
 * 作者:    1751400-张校
 * 功能:    处理命令行参数
 * 参数:
    int     argc:   同main函数参数argc
    char**  argv:   同main函数参数argv
 * 返回值:
 * 备注:    不用修改
 */
void CONFIG::Add_Config_By_Cmd_Argument(int argc, char **argv)
{
    int i = 1;
    while( i < argc )
    {
        if( i < argc && argv[i][0] == '-' && argv[i][1] == '-' )
        {
            if( i == argc - 1 || argv[i + 1][0] == '-' && argv[i + 1][1] == '-' )
            {
                items[&argv[i][2]] = "enable";
                i ++;
            }
            else
            {
                items[&argv[i][2]] = argv[i + 1];
                i += 2;
            }
        }
        else
        {
            i ++;
        }
    }
}

void CONFIG::Add_Config(string key, string value)
{
    items[key] = value;
}

string &CONFIG::Get_Config(string key)
{
    return items[key];
}

string &CONFIG::operator[](string key)
{
    return items[key];
}

void CONFIG::Erase(string key)
{
    items.erase(key);
}

void CONFIG::Erase_All()
{
    items.erase(items.begin(), items.end());
}

