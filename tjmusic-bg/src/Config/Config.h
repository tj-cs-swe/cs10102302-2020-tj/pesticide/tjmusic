/*
 * 文件名:  Config.h
 * 作者:    1751400-张校
 * 概述:    存放配置文件类CONFIG的定义
 */
#ifndef _CONFIG_H
#define _CONFIG_H

#include <iostream>
#include <fstream>
#include <string>
#include <map>

#define CONF_BUF_SIZE 1024

class CONFIG
{
private:
    std::map<std::string, std::string> items;
public:
    // 从文件中读取配置信息
    int Add_Config_By_File(const char *);
    // 从命令行参数中读取配置信息
    void Add_Config_By_Cmd_Argument(int, char **);
    // 添加配置信息
    void Add_Config(std::string, std::string);
    // 查询配置信息
    std::string& Get_Config(std::string);
    // 查询配置信息
    std::string& operator[](std::string);
    // 删除一条配置信息
    void Erase(std::string);
    // 删除所有配置信息
    void Erase_All();
};

#endif