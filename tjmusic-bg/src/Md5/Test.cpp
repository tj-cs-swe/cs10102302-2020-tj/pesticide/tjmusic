/*
 * 文件名:  Test.cpp
 * 作者:    1751400-张校
 * 概述:    可单独测试Md5
 */

#include "Md5.h"
#include <iostream>
using namespace std;

int main()
{
    string password;

    cin >> password;
    MD5 password_md5;
    password_md5.update(password);

    cout << password_md5.toString().c_str() << endl;
    return 0;
}