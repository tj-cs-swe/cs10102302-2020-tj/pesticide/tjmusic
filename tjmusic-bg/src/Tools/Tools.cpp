/*
 * 文件名:  Tools.cpp
 * 作者:    1751400-张校
 * 概述:    存放通用工具函数的实现
 */

#include "Tools.h"

using namespace std;

/* 在这里实现工具函数 */

/*
 * 函数:    int Daemon()
 * 作者:    1751400-张校
 * 功能:    创建守护进程
 * 参数:
 * 返回值: 
 * 备注:
 */
int Daemon()
{
    int pid, i;

    if((pid = fork()) > 0)
    {
        exit(0);
    }
    else if(pid < 0)
    {
        exit(1);
    }

    setsid();
    umask(0);
    return pid;
}

/*
 * 函数:    void sig_chld_handle(int n)
 * 作者:    1751400-张校
 * 功能:    信号SIG_CHLD的处理函数
 * 参数:
 * 返回值: 
 * 备注:
 */
void sig_chld_handle(int n)
{
    int status;
    while( waitpid(-1, &status, WNOHANG) > 0 );
    exit(0);
}

/*
 * 函数:    string Get_Time()
 * 作者:    1751400-张校
 * 功能:    获取当前（某一）时间的字符串表示
 * 参数:
    time_t  tt = 0: 时间戳，默认为当前时间
 * 返回值: 
 * 备注:
 */
string Get_Time(time_t tt)
{
    char buf[64];
    struct tm *ttime = localtime(&tt);
    strftime(buf, 64, "%Y-%m-%d %H:%M:%S", ttime);
    return string(buf);
}