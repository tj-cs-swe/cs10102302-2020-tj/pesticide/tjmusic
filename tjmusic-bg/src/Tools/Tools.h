/*
 * 文件名:  Tools.h
 * 作者:    1751400-张校
 * 概述:    存放通用工具函数
 * 备注:    这里定义的函数目前仅供Server文件夹中程序代码（后续也供下载程序）使用
 */

#ifndef _TOOLS_H
#define _TOOLS_H

#include <string>
#include <cstdlib>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

/* 在这里添加头文件 */



/* 在这里定义宏定义 */



/* 在这里定义数据类型和函数 */

// 创建守护进程
int Daemon();
// 信号处理函数
void sig_chld_handle(int);
// 时间戳转日期
std::string Get_Time(time_t = time(NULL));

#endif