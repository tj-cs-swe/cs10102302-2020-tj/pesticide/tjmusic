/*
 * 文件名:  Tester.cpp
 * 作者:    1751400-张校
 * 概述:    简单测试文件
 */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <map>
#include <set>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/epoll.h>
#include <limits.h>
#include <sys/file.h>
#include <sys/prctl.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/un.h>
#include <sys/wait.h>

#include "../Json/single_include/nlohmann/json.hpp"

using json = nlohmann::json;

using namespace std;

int main()
{
    int sock;
    struct sockaddr_in serv_addr;
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if( sock == -1 )
        return 0;

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(21345);
    if( connect(sock, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) == -1 )
    {
        cerr << "connect failed" << endl;
        return 0;
    }
    char buf[1000];

    if( 1 )
    {
        char buf[1000];
        
        json req = {
            {"type", "register"},
            {"username", "user"},
            {"password", "123456"},
            {"session", "eUg6IItYQGzADziBb1Too2tcArVb3Ubd"},
            {"connect", "keep alive"},
        };
        string str = req.dump();
        if( str.length() <= 0 )
            return 0;
        write(sock, str.c_str(), str.length());
        
        int len = read(sock, buf, 1000);
        if( len <= 0 )
            return 0;
        buf[len] = 0;
        req = json::parse(buf, nullptr, false);
        if( req.is_discarded() )
            return 0;
        cout << req << endl;
    }
    
    close(sock);
    return 0;
}

// int main()
// {
//     int sock;
//     struct sockaddr_in serv_addr;
//     sock = socket(PF_INET, SOCK_STREAM, 0);
//     if( sock == -1 )
//         return 0;

//     memset(&serv_addr, 0, sizeof(serv_addr));
//     serv_addr.sin_family = AF_INET;
//     serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
//     serv_addr.sin_port = htons(21345);
//     if( connect(sock, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) == -1 )
//     {
//         cerr << "connect failed" << endl;
//         return 0;
//     }
//     char buf[1000];

//     if( 1 )
//     {
//         char buf[1000];
        
//         json req = {
//             {"type", "user collection"},
//             {"username", "qqq"},
//             {"session" , "12345"}
//         };
//         string str = req.dump();
//         if( str.length() <= 0 )
//             return 0;
//         write(sock, str.c_str(), str.length());
        
//         int len = read(sock, buf, 1000);
//         if( len <= 0 )
//             return 0;
//         buf[len] = 0;
//         req = json::parse(buf, nullptr, false);
//         if( req.is_discarded() )
//             return 0;
//         cout << req << endl;
//     }
    
//     close(sock);
//     return 0;
// }