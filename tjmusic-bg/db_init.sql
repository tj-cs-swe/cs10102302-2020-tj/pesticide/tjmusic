/*
 * 在此存放数据库初始化代码
 * 运行 Server --db_build 则会运行此脚�? 
 */


CREATE DATABASE IF NOT EXISTS tjmusic;
USE tjmusic;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
    user_id         INT(4)      NOT NULL    AUTO_INCREMENT  PRIMARY KEY,
    user_name       CHAR(25)    NOT NULL,
    user_password   CHAR(40)    NOT NULL,
    last_login_time DATETIME    NOT NULL
);

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
    session_code    CHAR(32)    NOT NULL    PRIMARY KEY,
    user_id         INT(4)      NOT NULL    REFERENCES users(user_id),
    last_active_time DATETIME   NOT NULL
);

/*
 * 音乐
 * 保存爬到的音乐信息和下载地址
 */
DROP TABLE IF EXISTS musics;
CREATE TABLE musics (
    music_id        INT(4)      NOT NULL    AUTO_INCREMENT  PRIMARY KEY,
    music_name      CHAR(255)   NOT NULL, /* 名字 */
    music_subtitle  CHAR(255)   NOT NULL, /* 子标题 */
    singers         CHAR(255)   NOT NULL, /* 歌手列表 */
    album_name      CHAR(255)   NOT NULL, /* 专辑 */
    time_public     DATE        NOT NULL, /* 公开时间 */
    music_type      INT(4)      NOT NULL, /* 类型 */
    music_language  INT(4)      NOT NULL, /* 语言 */
    song_source     CHAR(20)    NOT NULL, /* 来源 */
    song_id         INT(4)      NOT NULL, /* 音乐在原平台的id */
    song_mid        CHAR(20)    NOT NULL, /* 音乐在原平台的mid */
    music_link      CHAR(255)   NOT NULL  /* 音乐链接 */
);

/*
 * 歌单
 * 每一个存在的歌单都必须在这张表中记录
 */
DROP TABLE IF EXISTS music_collections;
CREATE TABLE music_collections (
    collection_id   INT(4)      NOT NULL    AUTO_INCREMENT  PRIMARY KEY,
    collection_name      CHAR(255)   NOT NULL, 
    owner_user      INT(4)      NOT NULL    REFERENCES users(user_id),
    private         CHAR(32)    NOT NULL
);

/*
 * 歌单中的音乐
 * 记录歌单中存放的音乐，每一个歌单存放一首音乐就建立一项
 */
DROP TABLE IF EXISTS music_in_collection;
CREATE TABLE music_in_collection (
    collection_id   INT(4)      NOT NULL    REFERENCES music_collections(collection_id),
    music_id        INT(4)      NOT NULL    REFERENCES musics(music_id)
);

/*
 * 用户收藏歌单
 * 记录用户收藏的歌单，每一个用户收藏一个歌单建立一项
 */
DROP TABLE IF EXISTS user_collections;
CREATE TABLE user_collections (
    user_id         INT(4)      NOT NULL    REFERENCES users(user_id),
    collection_id   INT(4)      NOT NULL    REFERENCES music_collections(collection_id)
);
